// Bài 1: sắp xếp 3 số theo thứ tự tăng dần

function sapXepTangDan() {
    var number1 = document.getElementById("number1").value * 1;
    var number2 = document.getElementById("number2").value * 1;
    var number3 = document.getElementById("number3").value * 1;

    var resultEx1 = document.getElementById("result-ex-1");
    
    if (number1 < number2 && number2 < number3) {

        resultEx1.innerHTML = `${number1} < ${number2} < ${number3} `;

    } else if (number1 < number3 && number3 < number2) {

        resultEx1.innerHTML = `${number1} < ${number3} < ${number2} `;

    } else if (number2 < number1 && number1 < number3) {

        resultEx1.innerHTML = `${number2} < ${number1} < ${number3} `;

    } else if (number2 < number3 && number3 < number1) {

        resultEx1.innerHTML = `${number2} < ${number3} < ${number1} `;
 
    } else if (number3 < number1 && number1 < number2) {

        resultEx1.innerHTML = `${number3} < ${number1} < ${number2} `;

    } else {

        resultEx1.innerHTML = `${number3} < ${number2} < ${number1} `;
    }
}

// Bài 2: Chương trình chào hỏi

function chaoHoi() {
    var familyValue = document.getElementById("family").value;
    var resultEx2 = document.getElementById("result-ex-2");

    if (familyValue == "dady") {
        resultEx2.innerHTML = `Xin chào Bố!`
    } else if (familyValue == "mom") {
        resultEx2.innerHTML = `Xin chào Mẹ!`
    } else if (familyValue == "brother") {
        resultEx2.innerHTML = `Xin chào Anh trai!`
    } else {
        resultEx2.innerHTML = `Xin chào Em gái!`
    }
}

// Bài 3: Đếm số chẵn lẻ

function dem() {
    var number1El = document.getElementById("number1-ex3").value * 1; 
    var number2El = document.getElementById("number2-ex3").value * 1;
    var number3El = document.getElementById("number3-ex3").value * 1;
    
    var resultEx3 = document.getElementById("result-ex-3");

    var tongChan = 0

    if (number1El % 2 == 0) {
        tongChan = tongChan + 1;
    } 
    if (number2El % 2 == 0) {
        tongChan = tongChan + 1;
    } 
    if (number3El % 2 == 0) {
        tongChan = tongChan + 1;
    }

    resultEx3.innerHTML = ` Có ${tongChan} số chẵn; Có ${3 - tongChan} số lẻ`
}

// Bài 4: Dự doán tam giác

function duDoan() {
    var edge1 = document.getElementById("edge1").value * 1;
    var edge2 = document.getElementById("edge2").value * 1;
    var edge3 = document.getElementById("edge3").value * 1;

    var resultEx4 = document.getElementById("result-ex-4");

    if (edge1 == edge2 && edge1 == edge3) {
        resultEx4.innerHTML = `Hình tam giác Đều`;
    } else if (edge1 == edge2 || edge1 == edge3 || edge2 == edge3) {
        resultEx4.innerHTML = `Hình tam giác Cân`;
    } else if (edge1 * edge1 == edge2 * edge2 + edge3 * edge3 || edge2 * edge2 == edge1 * edge1 + edge3 * edge3 || edge3 * edge3 == edge1 * edge1 + edge2 * edge2) {
        resultEx4.innerHTML = `Hình tam giác Vuông`;
    } else {
        resultEx4.innerHTML = `Một loại tam Khác`;
    }
}